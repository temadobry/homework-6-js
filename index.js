// Экранирование позволяет внутри текста использовать знаки, которые зарезервированы для синтаксиса ( например "" внутри строки экранируются \)
// Function Declaretion, Function expression, Arrow function
//  Hoisting. Если функция заявлена через function declaretion (function sum() {})  может быть вызвана до того, как будет создана.   
//  Переменные также могут быть заявлены позже, чем будут вызваны, однако значения переменных читаются в той последовательности, в которой указаны.   
//  
 
"use strict"
const createNewUser = () => {
  const userName = prompt("Ваше ім'я");
  const userLastName = prompt("Ваше прізвище");
  const userBirthday = prompt("Повідомте Ваш вік в форматі dd.mm.yyyy")
  return {
    userName,
    userLastName,
    userBirthday,
    getLogin() {
      return `${this.userName[0]}${this.userLastName}`.toLowerCase()
    },
    getAge() {

      const today = new Date()
      const year = Number(userBirthday.slice(-4));
      const month = Number(userBirthday.slice(-7, -5)) - 1;
      const day = Number(userBirthday.slice(0, 2));
      let age = today.getFullYear() - year;
      if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) { age-- };
      return age

    },
    getPassword() {
      return `${this.userName[0].toUpperCase()}${this.userLastName.toLowerCase()}${userBirthday.slice(-4)}`
    },
  }
}

let newUser = createNewUser()
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
















